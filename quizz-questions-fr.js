/**
    @author Guillaume Camenzind
    @date feb 2014
    @rev 210220141413
    aBCM Quiz with jQuery
*/
default_bad_message = "Ceci n'est pas la bonne réponse!";
defauilt_good_message = "Bravo, vous avez trouvé!";
  
categories=[
  'Technologies',//0
  'Informatique suisse',//1
  'Calculatrices et pockets',//2
  'Micro-ordinateurs',//3
  'Consoles de jeux',//4
  ];
  
questions=[//category, question, answers[0,1,2,...], index of good answer[0|1|2|3], good answer message
  //Question 1
  [ 0,
    'Pour quelle application le premier micro-processeur a-t-il été conçu?',
    [
    'Pour le premier micro-ordinateur',
    'Pour piloter une fusée de la NASA',
    'Pour une calculatrice japonaise',
    'Pour piloter les robots industriels',
    ],
    2,
    "En 1969, la société japonaise Busicom demande à Intel un jeu de circuits intégrés pour réaliser une calculatrice. L'idée de base n'était pas de créer un microprocesseur et lorsque T. Hoff, S. Mazor et F. Faggin développent le 4004, ils n'imaginent pas l'impact futur de cette simple puce. Le 4004 sort officiellement en 1971. L'Intel 8080, en 1974, est le premier à avoir un réel succès. Les Motorola 6800 et Zilog Z80 suivront rapidement.",
  ],    
  //Question 2
  [ 0,
    "Quel support n'a pas été utilisé pour stocker des données informatiques?",
    [
    'Papier',
    'Disque vinyle',
    'Casstte audio',
    'Tambour magnétique',
    ],
    1,
    "Le disque vinyle, analogique et pressé une fois pour toute, n'a jamais été utilisé en informatique. Les cartes et bandes en papier perforées inventées au XIXe siècle pour le sautomates ont été utilisées jusque dans les années 70. Par la suite, les micro-ordinateurs ont longtemps exploité la cassette audio comme enregistrement magnétique; en effet, les disques durs et disquettes, dont l'ancêtre lointain est le tambour (1948), étaient extrêmement chers."
  ],
  [ 0,
    "Si une puce fabriquée en 2003, de la taille d'une pièce de 1 franc, était construite avec la technologie d'il y a 50 ans, quelle serait sa taille?",
    [
    "La taille de ce quiz",
    "La taille de la grand voile d'Alinghi",
    "La taille d'un terrain de football",
    "La taille de l'EPFL"
    ],
    2,
    "Un terrain de football. Dans les années 50, peu après la découverte des transistors, il fallait les assaembler individuellement. Plus tard, les puces (circuits intégrés) ont permis de concentrer plusieurs transistors dans le même composant. En 2003, on sait fabriquer des transistors de taille microscopique et on en met plusieurs dizaines de millions sur une puce de quelques centimètres carrés."
  ],
  [ 1,
    "Quelle personalité a donné son nom à un langage de programmation suisse?",
    [
    "Gustave Eiffel",
    "Richard Nixon",
    "Ada Lovelace",
    "Blaise Pascal"
    ],
    3,
    "Il s'agit de Blaise Pascal, mathématicien français. Niklaus Wirth, professeur à l'EPF de Zürich, met au point ce langage dans les années 60 pour arriver à une version définitivement 1971. Le Pascal sera très largement utilisé dans le monde entier, principalement comme langage d'apprentissage. Son extension, le Modula-2, servira de base au débeloppement de l'ordinateur suisse Lilith, construit en 1980."
  ],    
  [ 3,
    "En quelle année le premier prototype de la souris a-t-il été présenté?",
    [
    "1942",
    "1968",
    "1981",
    "1991",
    ],
    1,
    "C'est en 1968, après 5 ans de recherches à Stanford, que Douglas Engelbart présente le premier prototype de souris, ainsi qu'une série de concepts d'avant-garde tels que l'interface graphique, l'hypertexte et l'e-mail. À la fin des années 70, le LAMI du professeur Nicoud à l'EPFL développeune souris hémisphérique à boule; elle est commercialisée par l'entreprise romande Dépraz, puis plus tard par Logitech."
  ],    
  [ 2,
    "Quel langage de programmation équipait la plupart des ordinateurs de poche des années 80?",
    [
    "Pascal",
    "Basic",
    "Cobol",
    "Logo",
    ],
    1,
    "Il existe plusieurs centaines de langages de programmation. Parmis eux, Beginner's All-purpose Symbolic Instruction Code (BASIC) a été développé par J. Kemeny et T. Kurtz en 1964. D'un abord aisé, il était disponible sur presque tous les micro-ordinateurs et les pockets des années 80. En 1975, une petite société est fondée par P. Allen et W. Gates pour produire un interpréteur BASIC sur Altair. Cette entreprise porte le nom de Micro-soft."
  ],    
  [ 4,
    "Quel aurait pu être le nom de la société Atari?",
    [
    "Wysiwyg",
    "Foobar",
    "Syzygy",
    "Toto",
    ],
    2,
    "Afin de commercialiser le jeu vidéo qu'ils ont créé, N. Bushnell, T. Dabney et L. Bryan décident en 1972 de créer une société. Ils lui donnent le nom de Syzygy, un terme d'astronomie. Celui-ce étant déjà utilisé, ils choisiront Atari, mot japonais tiré du jeu de Go. Leur premier produit, la borne d'arcade Pong, est un immense succès. Pendant plus de vingt ans, Atari sera un acteur majeur dans le monde des jeux vidéo et de la micro-informatique."
  ],
];