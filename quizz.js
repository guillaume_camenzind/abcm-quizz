/**
    @author Guillaume Camenzind
    @date feb 2014
    @rev 210220141413
    aBCM Quiz with jQuery
*/
jQuery(document).ready(function() {
  
  CATEGORY=0;
  INTITULE = 1;//index of questio in array
  ANSWERS=2;
  GOOD_ANSWER = 3;//index of good answer in array
  HINT=4;
  
  score = 0;
  
  //add categories
  jQuery('.quiz').append('<div class="categories"></div>');
  jQuery.each(categories, function(i,v){
    var cat = 
    '<div class="category">'+
    '<h2>'+(i+1)+'. '+v+'</h2>'+
    '<div class="questions">'+
    '</div>'+
    "</div>";
    jQuery('.categories').append(cat);
  });  
  
  //add score
  jQuery('.quiz').append('<div class="score"><b>Votre score: 0/'+questions.length+'</b></div>');
  
  //generate quiz
  jQuery.each(questions, function(ind,question){
    var questionind = jQuery('.quiz > .categories > .category:eq('+question[CATEGORY]+') > .questions > .question').length +1;
    
    var answers = '';
    jQuery.each(question[ANSWERS], function(i, answer){
      answers += '<span class="answer"><p>'+(i+1)+'. '+answer+'</p></span>';
    });
    
    quest=
    '<div class="question" id="'+ind+'">'+    
    "<H3>Question "+questionind+":</H3>"+
    "<H4>"+question[INTITULE]+"</H4>"+
    '<div class="answers">'+answers+    
    "</div>"+
    '<div class="msg bad">'+
    '<p>'+default_bad_message+'</p>'+
    "</div>"+
    '<div class="msg good">'+
    '<p>'+defauilt_good_message+'</p>'+
    '<p class="hint">'+question[HINT]+'</p>'+
    "</div>"+
    "</div>";

    jQuery('.quiz > .categories > .category:eq('+question[CATEGORY]+')').children('.questions').append(quest);
  });
  
  //renum questions
  jQuery('.question').each(function(i,v){    
    jQuery(this).children('h2:eq(0)').html('Question '+(i+1)+' :');
  });
  
  //attach event
  jQuery('.answer').click(function(){
    var id = jQuery(this).parents('.question').attr('id');
    if(questions[id][GOOD_ANSWER] == jQuery(this).index()) {
      jQuery(this).parents('.question').children('.msg.good').show();
      jQuery(this).parents('.question').children('.msg.bad').hide();
      
      if(jQuery(this).parents('.question.answered').length == 0)
        majscore(1);
    }else{
      jQuery(this).parents('.question').children('.msg.bad').show();
      jQuery(this).parents('.question').children('.msg.good').hide();  
    }
    
    jQuery(this).parents('.question').addClass('answered');
  });
  
  majscore = function(i){
    score += i;
    jQuery('.score').html('<b>Votre score: '+score+'/'+questions.length+'</b>');
  };
});